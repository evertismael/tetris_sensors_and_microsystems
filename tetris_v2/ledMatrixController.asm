/*
 * ledMatrixController.asm
 *
 *  Created: 02/04/2018 02:07:04
 *   Author: Evert Ismael
 */ 

; -------------------------------------------------------
; inputs: 
;		Y			: is the address of the most Right byteColumn.
;		arg_A_in     : bit position (mask!)  R21
; output: arg_D_out
ledMarix_collectBufferByte:
	push YL
	push YH
	push cntA	; bit controller
	push var_A
	push var_B
	;----
	LDI var_B,0x0	; clean buffer
	LDI cntA,0x01
	ADIW Y,1			; correct the predecrement
L_NextBitBuffer:
	LD var_A, -Y
	SBIW Y,1
	AND var_A,arg_A_in	; takes care of correct bit pos
	CP var_A,arg_A_in	; if equal -> bit is 1
	BRNE L_checkLoopBuffer
L_bufferBitOne:	; set bit in the correct position of buffer
		SET
		SBRC cntA,0	;
		BLD var_B,0	; set bit at pos 0
		SBRC cntA,1	;
		BLD var_B,1	; pos 1
		SBRC cntA,2	;
		BLD var_B,2	; pos 2
		SBRC cntA,3	;
		BLD var_B,3	; pos 3
		SBRC cntA,4	;
		BLD var_B,4	; pos 4
		SBRC cntA,5	;
		BLD var_B,5	; pos 5
		SBRC cntA,6	;
		BLD var_B,6	; pos 6
		SBRC cntA,7	;
		BLD var_B,7	; pos 7
L_checkLoopBuffer:
	LSL cntA
	CPI cntA,0
	BRNE L_NextBitBuffer

	LDI YL,low(BUFFER_REG)
	LDI YH,high(BUFFER_REG)
	ST Y,var_B

	;----
	pop var_B
	pop var_A
	pop cntA 
	pop YH
	pop YL
RET


;-------------------------------------------------

ledMatrix_ShowData:
	push YL
	push YH
	push arg_A_in
	push arg_B_in
	push var_A
	;---
	LDI var_A, 0x80	; first row
L_nextRow:
	mov arg_A_in,var_A
	
	; Low part
	LDI arg_B_in, DRAW_DISP_L
	rcall ledMatrix_showHighOrLow

	; HIGH part
	LDI arg_B_in, DRAW_DISP_H
	rcall ledMatrix_showHighOrLow
	
	
	; Column part
	LDI arg_A_in,0x00

	;SBRC var_A,0	;
	;BLD  arg_A_in,0	; set bit at pos 0
	SBRC var_A,1	;
	BLD  arg_A_in,7	; pos 1
	SBRC var_A,2	;
	BLD  arg_A_in,6	; pos 2
	SBRC var_A,3	;
	BLD  arg_A_in,5	; pos 3
	SBRC var_A,4	;
	BLD  arg_A_in,4	; pos 4
	SBRC var_A,5	;
	BLD  arg_A_in,3	; pos 5
	SBRC var_A,6	;
	BLD  arg_A_in,2	; pos 6
	SBRC var_A,7	; row 8
	BLD  arg_A_in,1	; pos 7

	LDI YL,low(BUFFER_REG)
	LDI YH,high(BUFFER_REG)
	ST Y,arg_A_in
	rcall shiftReg_SendArgAToShiftRegisters	; ROWS enable - sends arg_A_in
	rcall shiftReg_PulseShiftLatch				; latch

L_checkRowMask:
	LSR var_A
	CPI var_A,0x01	
	BRNE L_nextRow		; iterate next row.
	;-----
L_end_ledMatrix_ShowData:
	rcall shiftReg_CleanRegister
	;---
	pop var_A
	pop arg_B_in
	pop arg_A_in
	pop YH
	pop YL
	
RET





; --------------------------------------------------------
; Sends 20 first or last bits of columns 
; input in arg_B_in = DRAW_DISP_L or DRAW_DISP_H
ledMatrix_showHighOrLow:
	push cntA
	push YL
	push YH
	push arg_A_in
	push var_A
	;-----
	LDI cntA, 5			; cause 8bytes -> 40 cols
	mov var_A, arg_A_in

	LDI YL,low(DISP)
	LDI YH, high(DISP)
	cpi arg_B_in,DRAW_DISP_L
		breq L_lowDispCols
	cpi arg_B_in,DRAW_DISP_H
		breq L_highDispCols
	rjmp GLOBAL_ERROR 
L_lowDispCols:
	ADIW Y,NUM_COLS					; points to last position
	ADIW Y,NUM_COLS					
	SBIW Y,2					
	rjmp L_nextBunchCol
L_highDispCols:
	ADIW Y,NUM_COLS					; points to last position
	ADIW Y,NUM_COLS					
	SBIW Y,1

L_nextBunchCol:	
	mov arg_A_in, var_A
	rcall ledMarix_collectBufferByte	; output in BUFFER_REG
	rcall shiftReg_SendArgAToShiftRegisters
	SBIW Y,16						; it moves to the next bunch of cols
	DEC cntA					; check if 5 bunches are alreqdy sent
	BRNE L_nextBunchCol
	;----
	pop var_A
	pop arg_A_in
	pop YH
	pop YL
	pop cntA
RET

;----------------------------------------
	
ledMatrix_EraseData:
	push XL
	push XH
	push var_A
	push var_B
	;---
	; erase the screen
	LDI XL,low(DISP)
	LDI XH, high(DISP)
	LDI var_A,NUM_COLS		; run it 48 times
	LDI var_B,0x00	;
L_deleteByteH:
	ST	X+,var_B	; byte 1
	ST	X+,var_B	; byte 1
	DEC var_A
	BRNE L_deleteByteH
	;----
	pop var_B
	pop var_A
	pop XH
	pop XL

RET

; --------- DUMMY DATA in DISP ----

ledMatrix_WriteDummyData:
	push XL
	push XH
	push var_B
	push var_A
	;---
	LDI XL,low(DISP)
	LDI XH, high(DISP)
	LDI var_A,NUM_COLS		; run it 48 times
	LDI var_A,16		; run it 48 times

L_dummyByte:
	LDI var_B,0x8C			; byte to send
	ST	X+,var_B	; first byte
	LDI var_B,0x33			; byte to send
	ST	X+,var_B	; second byte

	LDI var_B,0x40			; byte to send
	ST	X+,var_B	; first byte
	LDI var_B,0x44			; byte to send
	ST	X+,var_B	; second byte

	LDI var_B,0x66			; byte to send
	ST	X+,var_B	; first byte
	LDI var_B,0x77			; byte to send
	ST	X+,var_B	; second byte

	;ROL var_B
	DEC var_A
	BRNE L_dummyByte

	; lowest part limit
	LDI XL,low(GROUND)
	LDI XH, high(GROUND)
	LDI var_B,0xff	;
	ST	X+,var_B	; first byte
	ST	X+,var_B	; second byte
	;---
end_ledMatrix_WriteDummyData:
	pop var_A
	pop var_B
	pop XH
	pop XL
 RET

 ;------------------------------------------------
ledMarix_Test:
	push arg_A_in
	;---
	LDI arg_A_in,0x08
	rcall shiftReg_SendArgAToShiftRegisters
	LDI arg_A_in,0x00	
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters

	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	
	LDI arg_A_in,0x80	
	rcall shiftReg_SendArgAToShiftRegisters	
	rcall shiftReg_PulseShiftLatch
	;----
	pop arg_A_in
RET
