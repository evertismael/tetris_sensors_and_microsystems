/*
 * delay.asm
 *
 *  Created: 02/04/2018 01:52:36
 *   Author: Evert Ismael
 */ 
;----------------------------------------
delay:
	PUSH cntA
	;--
	ldi cntA,PULSE_DELAY
delay_loop:
	dec cntA
	brne delay_loop
	;--	
	POP cntA	
RET
;-----------------------------------------
delay_nested:
	PUSH cntA
	;--
	ldi cntA,PULSE_DELAY
delay_loop_nested:
	rcall delay
	dec cntA
	brne delay_loop_nested
	;--
	POP cntA	
RET

;----------------------------------------
delay_b:
	PUSH cntA
	;--
	ldi cntA,BUTTON_DELAY
delay_loop_b:
	dec cntA
	brne delay_loop_b
	;--	
	POP cntA	
RET
;------------------------------------------
delay_buttonNested:
	PUSH cntA
	;--
	ldi cntA,BUTTON_DELAY
delay_loop_buttonNested:
	rcall delay_b
	dec cntA
	brne delay_loop_buttonNested
	;--
	POP cntA	
RET
;------------------------------------------
delay_button:
	PUSH cntA
	;--
	ldi cntA,BUTTON_DELAY
delay_button_loop:
	rcall delay_buttonNested
	dec cntA
	brne delay_button_loop
	;--
	POP cntA	
RET
;------------------------------------------
delay_button_DOWN:
	PUSH cntA
	;--
	ldi cntA,BUTTON_DELAY_DOWN
delay_button_loop_d:
	rcall delay_buttonNested
	dec cntA
	brne delay_button_loop_d
	;--
	POP cntA	
RET
