/*
 * common.asm
 *
 *  Created: 03/04/2018 22:15:22
 *   Author: Evert Ismael
 */ 
 
 ; ----- Arithmetic Shifting to the Right for 14bit word -----------
 ; repeat the Arithmetic shifting several times.
 ; input registers: arg_B_in (MSB), arg_A_in(LSB)
 ; times to rotate. arg_C_in
 ; output registers: arg_E_out (MSB), arg_D_out(LSB)

com_rotRight14_ManyTimes:
	push var_A
	push var_B	
	push var_C
	;--
	mov var_A,arg_A_in
	mov var_B,arg_B_in
	mov var_C,arg_C_in

	cpi var_C,0
	breq L_finish	
L_rotateOnce:
	lsr var_A
	lsr var_B
	; correct if last bit in arg_B_in (MSB) has to go to LSB
	SET
	SBRC var_B,0
		BLD var_A,7
	dec var_C
	breq L_finish
	rjmp L_rotateOnce
L_finish:
	mov arg_D_out,var_A
	mov arg_E_out,var_B

L_endComRotRight14:
	;--
	pop var_C
	pop var_B	
	pop var_A;
 RET

;--------------------------------------
com_addAuxiliarWords:
	push cntB
	;--
	add aux_AH,aux_BH
	add aux_AL,aux_BL
	BRCC endAddAuxiliarWords ; if carry on
		ldi cntB,1
		add aux_AH,cntB
endAddAuxiliarWords:
	pop cntB
RET
