/*
 * LedMatrixController.asm
 *
 *  Created: 02/04/2018 01:48:59
 *   Author: Evert Ismael
 */ 

;-------------------------------
shiftReg_PulseShiftLatch:
	rcall delay_nested
	SBI PORTB,4		; rising edge.
	rcall delay_nested
	CBI PORTB,4		; falling.
	rcall delay_nested
RET

;----------------------------------------
shiftReg_SendOne:
	SBI PORTB,3
	SBI PORTB,5		; rising edge.
	CBI PORTB,5		; falling.
RET

;----------------------------------------
shiftReg_SendZero:
	CBI PORTB,3
	SBI PORTB,5		; rising edge.
	CBI PORTB,5		; falling.
RET

;-------------------------------------------------------
; input arg_A_in
shiftReg_SendArgAToShiftRegisters:
	push var_G		; reg for buffer.
	push cntA		; protect that register.
	push YL
	push YH
	;---

	LDI YL,low(BUFFER_REG)
	LDI YH,high(BUFFER_REG)
	LD var_G,Y

	LDI cntA,0x08	; iterate for every bit
L_NextBit:
		ROR var_G	; lsb to Carry
		BRCS sendOne	; if C=1 col led on
	SendZero:
			rcall shiftReg_SendZero
			RJMP L_ChkCtnBuffer
	SendOne:
			rcall shiftReg_SendOne 
L_ChkCtnBuffer:
	DEC cntA
	BRNE L_NextBit		; if not send next bit.

L_End_Helper_SendByteBuffer:
	;---
	pop YH
	pop YL
	pop cntA		
	pop var_G		
RET




; --------- CLEAN REGISTERS in DISP ----
shiftReg_CleanRegister:
	PUSH var_A
	push YL
	push YH
	;--
	LDI var_A,0;
	LDI YL,low(BUFFER_REG)
	LDI YH,high(BUFFER_REG)
	ST Y,var_A

	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
;	
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	rcall shiftReg_SendArgAToShiftRegisters
	
	rcall shiftReg_PulseShiftLatch
	;--
	pop YH
	pop YL
	POP var_A
 RET

 
 