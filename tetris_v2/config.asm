/*
 * config.asm
 *	Initial configuration of ports and other thigs Configuration
 *  Created: 28/03/2018 21:18:28
 *   Author: Evert Ismael
 */ 
;--------------------------------------------------------------------------------------
;------------------------------------ CONSTANTS ---------------------------------------
;--------------------------------------------------------------------------------------
.equ	TIMERCOUNT = 100	; 100  interruption per second for 1024 prescaler. 

.equ	NUM_COLS	=	48
.equ	PULSE_DELAY	=	0x40		; to delay the pulse while data is sent to the buffer.
.equ	BUTTON_DELAY=	0x2f		; 
.equ    BUTTON_DELAY_DOWN = 0x0f


.equ	MAX_ITER_FORCED_DOWN = 2		

.equ	DRAW_BLOCK = 0x01			; constant to make the code readable.
.equ	ERASE_BLOCK = 0x02			; constant to make the code readable.

.equ	DRAW_DISP_L = 0x00
.equ	DRAW_DISP_H = 0x01

.equ	LOCKED = 0x00			; constant for the screen.
.equ	UNLOCKED = 0x01			

.equ	MAX_X	=	13				; dimensions of the screen
.equ	MAX_Y	=	40				; dimensions of the screen

;--------------------------------------------------------------------------------------
;------------------------------------ DEFINITIONS -------------------------------------
;--------------------------------------------------------------------------------------

.def cntA	= R25		; counter for any loop
.def cntB	= R24		; counter for any loop

.def arg_A_in		= R23
.def arg_B_in		= R22
.def arg_C_in		= R21

.def arg_D_out		= R20
.def arg_E_out		= R19

.def var_A			= R18
.def var_B		 	= R17	
.def var_C			= R16	
.def var_D			= R15	
.def var_E			= R14	
.def var_F			= R13	
.def var_G			= R12	

.def aux_AL			= R11
.def aux_AH			= R10
.def aux_BL			= R9
.def aux_BH			= R8
.def SCREEN_STATUS	= R7	; if X,Y or Z are being modified cannot display the screen.

.def BLCK_X			= R6 	; initial X Block
.def BLCK_Y			= R5	; initial Y Block
.def BLCK_ID 		= R4	; relative
.def BLCK_ROT		= R3	; block rotation
.def BLCK_COLLIDE	= R2	; overlap-collide-in-map

.def ITER_COUNT	= R1    ; Iteration counter
;-----------------------------------------------------------------------------------
config_Ports:
	; testing led  
	SBI DDRC,2		; input->0, output->1
	SBI PORTC,2		; for Input => pull-up on->1 off->0 | for Output => H->1 L->0

	; controls 
	; Rows D7-4 as inputs; Columns D3-0 as outputs ;
	LDI R16, 0x0f	; input->0, output->1
	OUT DDRD, R16		

	LDI R16, 0xff	; for Input => pull-up on->1 off->0
					; for Output => H->1 L->0
	OUT PORTD,R16
RET
; --------- led matrix ----
config_LedMatrix:
	; LED MATRIX
	SBI DDRB,3		; Data To register as output.  input->0, output->1
	CBI PORTB,3		; Low value init.

	SBI DDRB,5		; Shift Data in register as output.  input->0, output->1
	CBI PORTB,5		; Low value init.

	SBI DDRB,4		; Shift registers latch as output.  input->0, output->1
	CBI PORTB,4		; Low value init.
RET

config_Interrupt:  
	PUSH R16
	;--
	; preescaler. 1024
	LDI R16,0x05	;101
	OUT TCCR0B,R16	

	LDI R16,0x00	
	OUT TCCR0A,R16	

	;timer config
	LDI R16,TIMERCOUNT	
	OUT TCNT0,R16

	SEI			; enabling global interrupt
	LDI R16,0x01
	STS TIMSK0,R16 ; enabling timer0 interrupt
	;--
	POP R16
RET


