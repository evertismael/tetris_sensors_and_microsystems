/*
 * tetrisBlocks.asm
 *
 *  Created: 02/04/2018 04:12:38
 *   Author: Evert Ismael
 */ 
 ;-------------------------------
 ;--------------------------------
 tetrisBlocks_Draw:
	push XL
	push XH
	push ZL
	push ZH
	push cntA
	push var_A
	push var_B
	push var_C
	push var_D
	push var_E
	push var_F
	;----
	mov var_A,arg_C_in

	; pointer block in Z
	rcall tetrisBlocks_getBlockPointerInZ	
	mov ZL,aux_BL
	mov ZH,aux_BH

	; pointer screen in Xreg, block_y
	rcall tetrisBlocks_getStartingRawScreenPointerInX	
	mov XL,aux_AL
	mov XH,aux_AH

	LDI cntA,8		; cause one block is 8 rows
   L_writeRow:
		; reading block
		LDI var_B,0
		MOV var_D,var_B		; upper part always zero at the begining
		LPM var_C,Z+

		; correction X.
		mov var_B,BLCK_X
		cpi var_B,0
		breq L_drawingDecision
		L_rotX:
			lsr var_D
			lsr var_C
			SET
			SBRC var_C,0
				BLD var_D,7
			dec var_B
			brne L_rotX

		L_drawingDecision:
		; decide if drawing or erasing
		cpi var_A,DRAW_BLOCK
		breq L_drawBock
		L_eraseBlock:
			LD var_E,X+; reading screen
			LD var_F,X
			com var_D
			com var_C
			and var_C,var_E
			and var_D,var_F
			rjmp  L_write_in_dip
		L_drawBock:
			LD var_E,X+; reading screen
			LD var_F,X
			or var_C,var_E
			or var_D,var_F		
	L_write_in_dip:
		ST X,var_D
		ST -X,var_C		
		ADIW X,2		; next row
		dec cntA
	brne L_writeRow
end_Draw:
	;----
	pop var_F
	pop var_E
	pop var_D
	pop var_C
	pop var_B
	pop var_A
	pop cntA
	pop ZH
	pop ZL
	pop XH
	pop XL
 RET

tetrisBlocks_getStartingRawScreenPointerInX:
	push var_A
	push XL
	push XH
	;--
	LDI XL,low(DISP)
	LDI XH,high(DISP)

	mov var_A,BLCK_Y
	add var_A,BLCK_Y
	cpi var_A,0
	breq end_getStartingRawScreenPointerInX
	;
add_oneMore:
	ADIW X,1
	dec var_A
	brne add_oneMore
	
end_getStartingRawScreenPointerInX:
	mov aux_AL,XL
	mov aux_AH,XH
	;---
	pop XH
	pop XL
	pop var_A
RET
;-------------------------------------
; output in Z
tetrisBlocks_getBlockPointerInZ:
	push var_A
	push XH
	push XL
	push ZH
	push ZL
	;--
	mov var_A,BLCK_ID

	LDI ZL,low(BLOCKS<<1)
	LDI ZH,high(BLOCKS<<1)	
	
	CPI var_A,0
	BREQ L_correctRot	; check id not zero
				
L_movePointer:
	ADIW Z,32			; 32 bytes per block
	DEC var_A				; check if in correct block 
	BRNE L_movePointer
	; rot

L_correctRot:
	mov var_A,BLCK_ROT
	CPI var_A,0
	BREQ L_end_tetrisBlocks_getBlockPointer

L_movePointerRot:
	ADIW Z,8			; 32 bytes per block
	DEC var_A
	BRNE L_movePointerRot
	
L_end_tetrisBlocks_getBlockPointer:
	mov aux_BL,ZL
	mov aux_BH,ZH
	;--
	pop ZL
	pop ZH
	pop XL
	pop XH
	pop var_A
RET

;-------------------------------
 ;--------------------------------
 tetrisBlocks_EraseFullLines:
	push XL
	push XH
	push ZL
	push ZH
	push cntA
	push var_A
	push var_B
	push var_C
	push var_D
	push var_E
	push var_F

	;----
	; pointer screen last row 
	LDI XL,low(DISP)
	LDI XH,high(DISP)
	adiw X,NUM_COLS
	adiw X,NUM_COLS
	sbiw X,2

	LDI cntA,44		; cause 48 rows in screen
    L_checkFullRow:
		LD var_B,X+; reading screen
		LD var_C,X
		SET
			BLD var_B,0
		SET
			BLD var_C,0
		and var_B,var_C
		cpi var_B,0xff
		brne L_checkLoopCond
		
		mov ZH,XH
		mov ZL,XL
		sbiw Z,1	
		mov var_A,cntA
		subi var_A,2
		L_eraseFullRow:
			sbiw Z,2			
			LD var_D,Z+
			LD var_E,Z+

			ST Z+,var_D
			ST Z,var_E
			sbiw Z,3
			
			dec var_A
			brne L_eraseFullRow
		SBIW X,1
		rjmp L_checkFullRow

		L_checkLoopCond:			
			SBIW X,3		; next row
			dec cntA
	brne L_checkFullRow
end_EraseFullLinesw:
	;----
	pop var_F
	pop var_E
	pop var_D
	pop var_C
	pop var_B
	pop var_A
	pop cntA
	pop ZH
	pop ZL
	pop XH
	pop XL
 RET