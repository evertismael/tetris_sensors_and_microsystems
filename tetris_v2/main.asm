; SensorsAndMicrosystemsProject_microcontrollers.asm
; Project Description: TETRIS simple TETRIS
; Created: 28/03/2018 21:02:48
; Author : Evert Ismael
;

; Definition file of the ATmega329P
.include "m328pdef.inc"
 ; Boot code (microcontroller starts @ address 0x0000)
.org 0x0000
	rjmp init

.org 0x0020
	rjmp InterruptionHandler
InterruptionHandler:
	push var_B
	;--
	mov var_B,SCREEN_STATUS
	cpi var_B,LOCKED
	breq L_endInterrupt
		rcall ledMatrix_ShowData
	L_endInterrupt:
		LDI var_B,TIMERCOUNT	
		OUT TCNT0,var_B
	;-----
	pop var_B
RETI

init:	;------------ Initialization ----------
		rcall config_Ports
		rcall config_Interrupt
		rcall config_LedMatrix
		rcall ledMatrix_WriteDummyData
		rcall ledMatrix_EraseData
		
		ldi var_A,MAX_ITER_FORCED_DOWN;
		mov  ITER_COUNT,var_A

		
		ldi var_A,0;
		mov  BLCK_ROT,var_A		
		
		ldi var_A,0;
		mov  BLCK_ID,var_A

		ldi var_A,6;
		mov  BLCK_X,var_A

		ldi var_A,0;
		mov  BLCK_Y,var_A
		

		rjmp main
				
main:	;------------ Main Program -------
	waitInput:
			LDI var_C, 0xfe	
			OUT PORTD, var_C
			nop
			nop
			SBIS PIND,7		; row 3 on -> is pressed
				rjmp ROTATE					
			SBIS PIND,6		; row 2 on -> is pressed	
				rjmp RIGHT				
			SBIS PIND,5		; row 1 on -> is pressed	
				rjmp DOWN				
			SBIS PIND,4		; row 0 on -> is pressed
				rjmp LEFT				
			rjmp checkIteration
		ROTATE:
			rcall buttons_Rotate
			rcall delay_button
			rjmp main 
		RIGHT:
			rcall buttons_Right
			rcall buttons_Right
			rcall delay_button
			rjmp main 
		DOWN:
			rcall buttons_Down
			rcall buttons_Down
			rcall delay_button_DOWN
			rjmp main 
		LEFT:
			rcall buttons_Left
			rcall buttons_Left
			rcall delay_button
			rjmp main

	checkIteration:			
			mov var_C,ITER_COUNT
			dec var_C
			breq L_forceDown			; equal ZERO go down
				dec ITER_COUNT
				rcall delay_button_DOWN
				rcall delay_button_DOWN
				rjmp L_endMain
			L_forceDown:
				ldi var_C,MAX_ITER_FORCED_DOWN;
				mov  ITER_COUNT,var_C
				rcall buttons_Down 
				rcall buttons_Down			
		L_endMain:
			rjmp main

GLOBAL_ERROR:
		RJMP GLOBAL_ERROR

; -------------------------------------------------------------------
;------------------- Block functions ---------------------------
; -------------------------------------------------------------------


; -------------------------------------------------------------------
;------------------- Include My Functions ---------------------------
; -------------------------------------------------------------------





.include "config.asm"			; Basic Rutines
.include "delay.asm"		; 
.include "ledMatrixController.asm"		;
.include "shiftRegisterController.asm"		;  
.include "common.asm"		;
.include "tetrisBlocks.asm"		;
.include "buttons.asm"		;
.include "blockObject.asm"		;


.include "block_def.asm"		; 
/*code memory data*/

.DSEG
	DISP: .BYTE 96	; display two bytes per column
	GROUND: .BYTE 2	; limits the display in the lower part
	
	BUFFER_REG:	.BYTE 1		; Byte to send to the shift registers.
