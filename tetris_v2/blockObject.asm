/*
 * blockObject.asm
 *
 *  Created: 27/04/2018 02:18:28
 *   Author: Evert Ismael
 */ 
 
;============================
;============================

blockObject_addY:
	push var_B
	;-------
	ldi var_B,1
	add var_B,BLCK_Y
	mov BLCK_Y,var_B
	;------
	pop var_B
RET

blockObject_addX:
	push var_B
	;-------
	ldi var_B,1
	add var_B,BLCK_X
	mov BLCK_X,var_B
	;------
	pop var_B
RET

blockObject_subX:
	push var_B
	;-------
	ldi var_B,-1
	add var_B,BLCK_X
	mov BLCK_X,var_B
	;------
	pop var_B
RET

;-----------------------------
blockObject_New:
	push var_A
	;--------------
	mov var_A,BLCK_Y
	cpi var_A,10;15
	brge next_new_block
	rcall ledMatrix_EraseData ; erase the game and start over

next_new_block:
	LDI var_A,6
	MOV BLCK_X, var_A	; initial X Block
	LDI var_A,5
	MOV BLCK_Y, var_A

	mov var_A,BLCK_ID
	cpi var_A,6
	breq resetID

	LDI var_A,1
	add var_A,BLCK_ID
	MOV BLCK_ID,var_A	; block id
	rjmp setRot
resetID:
	LDI var_A,0
	MOV BLCK_ID,var_A

setRot:
	LDI var_A,0
	MOV BLCK_ROT,var_A	; block rotation

	;------
	pop var_A
RET
;-----------------------------------------
blockObject_CheckCollide_DOWN:
	push XL
	push XH
	push ZL
	push ZH
	push cntA
	push var_A
	push var_B
	push var_C
	push var_D
	push var_E
	push var_F
	;----
	mov var_A,arg_C_in

	; pointer block in Z
	rcall tetrisBlocks_getBlockPointerInZ	
	mov ZL,aux_BL
	mov ZH,aux_BH
	adiw Z,7

	; pointer screen in Xreg, block_y
	rcall tetrisBlocks_getStartingRawScreenPointerInX	
	mov XL,aux_AL
	mov XH,aux_AH
	adiw X,14+2		; analize future row.


	LDI cntA,8		; cause one block is 8 rows
    L_checkRow:
		; reading block
		LDI var_B,0
		MOV var_D,var_B		; upper part always zero at the begining
		LPM var_C,Z
		sbiw Z,1

		; correction X.
		mov var_B,BLCK_X
		cpi var_B,0
		breq L_collideCheckDown
		L_rotXR:
			lsr var_D
			lsr var_C
			SET
			SBRC var_C,0
				BLD var_D,7
			dec var_B
			brne L_rotXR
		
		L_collideCheckDown:
		LD var_E,X+; reading screen
		LD var_F,X
		sbiw X,3
		; check collide downwards.
		and var_C,var_E
		and var_D,var_F
		or var_C,var_D
		cpi var_C,0
		breq L_NoCollideDownWrite	; if ZERO no collide downwards
		rjmp L_CollideDownWrite
		
		L_NoCollideDownWrite:
			ldi var_B,0
			mov BLCK_COLLIDE,var_B
			rjmp L_checkLoopD
		L_CollideDownWrite:
			ldi var_B,1
			mov BLCK_COLLIDE,var_B
			rjmp end_CollideDown
	L_checkLoopD:
		dec cntA
	brne L_checkRow
end_CollideDown:
	;----
	pop var_F
	pop var_E
	pop var_D
	pop var_C
	pop var_B
	pop var_A
	pop cntA
	pop ZH
	pop ZL
	pop XH
	pop XL
RET
;----------------------------------------------------------
;-----------------------------------------
blockObject_CheckCollide_RIGHT:
	push XL
	push XH
	push ZL
	push ZH
	push cntA
	push var_A
	push var_B
	push var_C
	push var_D
	push var_E
	push var_F
	;----
	mov var_A,arg_C_in

	; pointer block in Z
	rcall tetrisBlocks_getBlockPointerInZ	
	mov ZL,aux_BL
	mov ZH,aux_BH
	adiw Z,7

	; pointer screen in Xreg, block_y
	rcall tetrisBlocks_getStartingRawScreenPointerInX	
	mov XL,aux_AL
	mov XH,aux_AH
	adiw X,14		; analize future row.


	LDI cntA,8		; cause one block is 8 rows
    L_checkRow_R:
		; reading block
		LDI var_B,0
		MOV var_D,var_B		; upper part always zero at the begining
		LPM var_C,Z
		sbiw Z,1

		; correction X.
		ldi var_B,1		; one extra to analize next x
		add var_B,BLCK_X
		
		cpi var_B,0
		breq L_collideCheckRight
		L_rotXD:
			lsr var_D
			lsr var_C
			SET
			SBRC var_C,0
				BLD var_D,7
			dec var_B
			brne L_rotXD
		
		L_collideCheckRight:
		LD var_E,X+; reading screen
		LD var_F,X
		sbiw X,3
		SET
		BLD var_F,0
		CLT
		BLD var_E,0
		CLT
		BLD var_C,0
		
		; check collide Right.
		
		and var_C,var_E
		and var_D,var_F
		or var_C,var_D
		cpi var_C,0
		breq L_NoCollideRightWrite	; if ZERO no collide downwards
		rjmp L_CollideRightWrite
		
		L_NoCollideRightWrite:
			ldi var_B,0
			mov BLCK_COLLIDE,var_B
			rjmp L_checkLoopR
		L_CollideRightWrite:
			ldi var_B,1
			mov BLCK_COLLIDE,var_B
			rjmp end_CollideRight
	L_checkLoopR:
		dec cntA
	brne L_checkRow_R
end_CollideRight:
	;----
	pop var_F
	pop var_E
	pop var_D
	pop var_C
	pop var_B
	pop var_A
	pop cntA
	pop ZH
	pop ZL
	pop XH
	pop XL
RET
;--------------------------------------------------------------------------------


;-----------------------------------------
blockObject_CheckCollide_LEFT:
	push XL
	push XH
	push ZL
	push ZH
	push cntA
	push var_A
	push var_B
	push var_C
	push var_D
	push var_E
	push var_F
	;----
	mov var_A,arg_C_in

	; pointer block in Z
	rcall tetrisBlocks_getBlockPointerInZ	
	mov ZL,aux_BL
	mov ZH,aux_BH
	adiw Z,7

	; pointer screen in Xreg, block_y
	rcall tetrisBlocks_getStartingRawScreenPointerInX	
	mov XL,aux_AL
	mov XH,aux_AH
	adiw X,14		; analize  row.


	LDI cntA,8		; cause one block is 8 rows
    L_checkRow_L:
		; reading block
		LDI var_B,0
		MOV var_D,var_B		; upper part always zero at the begining
		LPM var_C,Z
		sbiw Z,1

		; correction X.
		mov var_B,BLCK_X  ; one extra to analize next x
		cpi var_B,0
		breq L_CollideLeftWrite
		ldi var_B,-1
		add var_B,BLCK_X

		cpi var_B,0
		breq L_collideCheckLeft
		L_rotXL:
			lsr var_D
			lsr var_C
			SET
			SBRC var_C,0
				BLD var_D,7
			dec var_B
			brne L_rotXL
		
		L_collideCheckLeft:
		LD var_E,X+; reading screen
		LD var_F,X
		sbiw X,3
		SET
		BLD var_F,0
		CLT
		BLD var_E,0		
		
		; check collide Right.		
		and var_C,var_E
		and var_D,var_F
		or var_C,var_D
		cpi var_C,0
		breq L_NoCollideLeftWrite	; if ZERO no collide downwards
		rjmp L_CollideLeftWrite
		
		L_NoCollideLeftWrite:
			ldi var_B,0
			mov BLCK_COLLIDE,var_B
			rjmp L_checkLoopL
		L_CollideLeftWrite:
			ldi var_B,1
			mov BLCK_COLLIDE,var_B
			rjmp end_CollideLeft
	L_checkLoopL:
		dec cntA
	brne L_checkRow_L
end_CollideLeft:
	;----
	pop var_F
	pop var_E
	pop var_D
	pop var_C
	pop var_B
	pop var_A
	pop cntA
	pop ZH
	pop ZL
	pop XH
	pop XL
RET
;--------------------------------------------------------------------------------
; ROTATION!!!!!!!!!!!!!!!!!!!
;-----------------------------------------
blockObject_ROT_CheckCollide_DOWN:
	push XL
	push XH
	push ZL
	push ZH
	push cntA
	push var_A
	push var_B
	push var_C
	push var_D
	push var_E
	push var_F
	;----
	mov var_A,arg_C_in

	; pointer block in Z
	rcall tetrisBlocks_getBlockPointerInZ	
	mov ZL,aux_BL
	mov ZH,aux_BH
	adiw Z,7

	; pointer screen in Xreg, block_y
	rcall tetrisBlocks_getStartingRawScreenPointerInX	
	mov XL,aux_AL
	mov XH,aux_AH
	adiw X,14		; analize future row.


	LDI cntA,8		; cause one block is 8 rows
    L_checkRow_ROT:
		; reading block
		LDI var_B,0
		MOV var_D,var_B		; upper part always zero at the begining
		LPM var_C,Z
		sbiw Z,1

		; correction X.
		mov var_B,BLCK_X
		cpi var_B,0
		breq L_collideCheckDown_ROT
		L_rotXR_ROT:
			lsr var_D
			lsr var_C
			SET
			SBRC var_C,0
				BLD var_D,7
			dec var_B
			brne L_rotXR_ROT
		
		L_collideCheckDown_ROT:
		LD var_E,X+; reading screen
		LD var_F,X
		sbiw X,3
		; check collide downwards.
		and var_C,var_E
		and var_D,var_F
		or var_C,var_D
		cpi var_C,0
		breq L_NoCollideDownWrite_ROT	; if ZERO no collide downwards
		rjmp L_CollideDownWrite_ROT
		
		L_NoCollideDownWrite_ROT:
			ldi var_B,0
			mov BLCK_COLLIDE,var_B
			rjmp L_checkLoopD_ROT
		L_CollideDownWrite_ROT:
			ldi var_B,1
			mov BLCK_COLLIDE,var_B
			rjmp end_CollideDown_ROT
	L_checkLoopD_ROT:
		dec cntA
	brne L_checkRow_ROT
end_CollideDown_ROT:
	;----
	pop var_F
	pop var_E
	pop var_D
	pop var_C
	pop var_B
	pop var_A
	pop cntA
	pop ZH
	pop ZL
	pop XH
	pop XL
RET
;----------------------------------------------------------
;-----------------------------------------
blockObject_ROT_CheckCollide_RIGHT:
	push XL
	push XH
	push ZL
	push ZH
	push cntA
	push var_A
	push var_B
	push var_C
	push var_D
	push var_E
	push var_F
	;----
	mov var_A,arg_C_in

	; pointer block in Z
	rcall tetrisBlocks_getBlockPointerInZ	
	mov ZL,aux_BL
	mov ZH,aux_BH
	adiw Z,7

	; pointer screen in Xreg, block_y
	rcall tetrisBlocks_getStartingRawScreenPointerInX	
	mov XL,aux_AL
	mov XH,aux_AH
	adiw X,14		; analize row.


	LDI cntA,8		; cause one block is 8 rows
    L_checkRow_R_ROT:
		; reading block
		LDI var_B,0
		MOV var_D,var_B		; upper part always zero at the begining
		LPM var_C,Z
		sbiw Z,1

		; correction X.
		mov var_B,BLCK_X
		
		cpi var_B,0
		breq L_collideCheckRight_ROT
		L_rotXD_ROT:
			lsr var_D
			lsr var_C
			SET
			SBRC var_C,0
				BLD var_D,7
			dec var_B
			brne L_rotXD_ROT
		
		L_collideCheckRight_ROT:
		LD var_E,X+; reading screen
		LD var_F,X
		sbiw X,3
		SET
		BLD var_F,0
		CLT
		BLD var_E,0
		CLT
		BLD var_C,0
		
		; check collide Right.
		
		and var_C,var_E
		and var_D,var_F
		or var_C,var_D
		cpi var_C,0
		breq L_NoCollideRightWrite_ROT	; if ZERO no collide downwards
		rjmp L_CollideRightWrite_ROT
		
		L_NoCollideRightWrite_ROT:
			ldi var_B,0
			mov BLCK_COLLIDE,var_B
			rjmp L_checkLoopR_ROT
		L_CollideRightWrite_ROT:
			ldi var_B,1
			mov BLCK_COLLIDE,var_B
			rjmp end_CollideRight_ROT
	L_checkLoopR_ROT:
		dec cntA
	brne L_checkRow_R_ROT
end_CollideRight_ROT:
	;----
	pop var_F
	pop var_E
	pop var_D
	pop var_C
	pop var_B
	pop var_A
	pop cntA
	pop ZH
	pop ZL
	pop XH
	pop XL
RET
;--------------------------------------------------------------------------------


;-----------------------------------------
blockObject_ROT_CheckCollide_LEFT:
	push XL
	push XH
	push ZL
	push ZH
	push cntA
	push var_A
	push var_B
	push var_C
	push var_D
	push var_E
	push var_F
	;----
	mov var_A,arg_C_in

	; pointer block in Z
	rcall tetrisBlocks_getBlockPointerInZ	
	mov ZL,aux_BL
	mov ZH,aux_BH
	adiw Z,7

	; pointer screen in Xreg, block_y
	rcall tetrisBlocks_getStartingRawScreenPointerInX	
	mov XL,aux_AL
	mov XH,aux_AH
	adiw X,14		; analize future row.


	LDI cntA,8		; cause one block is 8 rows
    L_checkRow_L_ROT:
		; reading block
		LDI var_B,0
		MOV var_D,var_B		; upper part always zero at the begining
		LPM var_C,Z
		sbiw Z,1

		; correction X.
		mov var_B,BLCK_X  ; one extra to analize next x
		cpi var_B,0
		breq L_collideCheckLeft_ROT
		ldi var_B,-1
		add var_B,BLCK_X

		cpi var_B,0
		breq L_collideCheckLeft_ROT
		L_rotXL_ROT:
			lsr var_D
			lsr var_C
			SET
			SBRC var_C,0
				BLD var_D,7
			dec var_B
			brne L_rotXL_ROT
		
		L_collideCheckLeft_ROT:
		LD var_E,X+; reading screen
		LD var_F,X
		sbiw X,3
		SET
		BLD var_F,0
		CLT
		BLD var_E,0		
		
		; check collide Right.		
		and var_C,var_E
		and var_D,var_F
		or var_C,var_D
		cpi var_C,0
		breq L_NoCollideLeftWrite_ROT	; if ZERO no collide downwards
		rjmp L_CollideLeftWrite_ROT
		
		L_NoCollideLeftWrite_ROT:
			ldi var_B,0
			mov BLCK_COLLIDE,var_B
			rjmp L_checkLoopL_ROT
		L_CollideLeftWrite_ROT:
			ldi var_B,1
			mov BLCK_COLLIDE,var_B
			rjmp end_CollideLeft_ROT
	L_checkLoopL_ROT:
		dec cntA
	brne L_checkRow_L_ROT
end_CollideLeft_ROT:
	;----
	pop var_F
	pop var_E
	pop var_D
	pop var_C
	pop var_B
	pop var_A
	pop cntA
	pop ZH
	pop ZL
	pop XH
	pop XL
RET