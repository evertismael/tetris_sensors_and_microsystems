/*
 * buttons.asm
 *
 *  Created: 27/04/2018 01:53:37
 *   Author: Evert Ismael
 */ 
buttons_Rotate:
	push var_A
	push arg_C_in
	;------
	LDI var_A,LOCKED
	mov SCREEN_STATUS,var_A
	;
	LDI arg_C_in,ERASE_BLOCK	; ERASE the block	
	rcall tetrisBlocks_Draw
	
	mov var_A,BLCK_ROT
	cpi var_A,3
	breq L_set_rot0
	L_add_rot:
		ldi var_A,1
		add var_A,BLCK_ROT
		mov BLCK_ROT,var_A
		rjmp L_checkCollide
	L_set_rot0:
		ldi var_A,0
		mov BLCK_ROT,var_A
	
	L_checkCollide:
		;right
		rcall blockObject_ROT_CheckCollide_RIGHT
		mov var_A,BLCK_COLLIDE
		cpi var_A,0
		brne L_UndoIncreaseRotation

		;left
		rcall blockObject_ROT_CheckCollide_LEFT
		mov var_A,BLCK_COLLIDE
		cpi var_A,0
		brne L_UndoIncreaseRotation

		;down
		rcall blockObject_ROT_CheckCollide_DOWN
		mov var_A,BLCK_COLLIDE
		cpi var_A,0
		brne L_UndoIncreaseRotation
		; if no collide jump to draw
		rjmp L_drawBlockAfterRotation
	L_UndoIncreaseRotation:
		mov var_A,BLCK_ROT
		cpi var_A,0
		breq L_set_rot3
		L_sub_rot:
			mov var_A,BLCK_ROT
			subi var_A,1
			mov BLCK_ROT,var_A
			rjmp L_drawBlockAfterRotation
		L_set_rot3:
			ldi var_A,3
			mov BLCK_ROT,var_A

	L_drawBlockAfterRotation:
	LDI arg_C_in,DRAW_BLOCK		; DRAW the block	
	rcall tetrisBlocks_Draw
	;
	LDI var_A,UNLOCKED
	mov SCREEN_STATUS,var_A
	;----
	pop arg_C_in
	pop var_A
RET 
;------------------------------------

buttons_Right:
	push var_A
	push arg_C_in
	;------
	LDI var_A,LOCKED
	mov SCREEN_STATUS,var_A
	;
	LDI arg_C_in,ERASE_BLOCK	; ERASE the block	
	rcall tetrisBlocks_Draw

	rcall blockObject_CheckCollide_RIGHT
	mov var_A,BLCK_COLLIDE
	cpi var_A,0
	brne L_endButtonsRight
	L_NOCollideRight:
		rcall blockObject_addX;
		rjmp L_endButtonsRight
  L_endButtonsRight:
	LDI arg_C_in,DRAW_BLOCK		; DRAW the block	
	rcall tetrisBlocks_Draw
	;
	LDI var_A,UNLOCKED
	mov SCREEN_STATUS,var_A
	;----
	pop arg_C_in
	pop var_A
RET 
;----------------------------------
buttons_Down:
	push var_A
	push arg_C_in
	;------
	LDI var_A,LOCKED
	mov SCREEN_STATUS,var_A
	;
	LDI arg_C_in,ERASE_BLOCK	; ERASE the block	
	rcall tetrisBlocks_Draw

	rcall blockObject_CheckCollide_DOWN
	mov var_A,BLCK_COLLIDE
	cpi var_A,0
	breq L_NOCollideDown
	L_CollideDown:
		

		LDI arg_C_in,DRAW_BLOCK		; DRAW the block	
		rcall tetrisBlocks_Draw
		
		rcall tetrisBlocks_EraseFullLines


		rcall blockObject_New
		rjmp L_endButtonsDown	
	L_NOCollideDown:
		rcall blockObject_addY;
		rjmp L_endButtonsDown
L_endButtonsDown:
	LDI arg_C_in,DRAW_BLOCK		; DRAW the block	
	rcall tetrisBlocks_Draw
	;
	LDI var_A,UNLOCKED
	mov SCREEN_STATUS,var_A
	;----
	pop arg_C_in
	pop var_A
RET
;-----------------------------------
buttons_Left:
	push var_A
	push arg_C_in
	;------
	LDI var_A,LOCKED
	mov SCREEN_STATUS,var_A
	;
	LDI arg_C_in,ERASE_BLOCK	; ERASE the block	
	rcall tetrisBlocks_Draw

	rcall blockObject_CheckCollide_LEFT
	mov var_A,BLCK_COLLIDE
	cpi var_A,0
	brne L_endButtonsLeft
	L_NOCollideLeft:
		rcall blockObject_subX;
		rjmp L_endButtonsLeft
  L_endButtonsLeft:
	LDI arg_C_in,DRAW_BLOCK		; DRAW the block	
	rcall tetrisBlocks_Draw
	;
	LDI var_A,UNLOCKED
	mov SCREEN_STATUS,var_A
	;----
	pop arg_C_in
	pop var_A
RET	
